package com.mar.manage.catalog.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mar.manage.catalog.dto.CategoryDTO;
import com.mar.manage.catalog.dto.MapperDTO;
import com.mar.manage.catalog.exception.CategoryNotFoundException;
import com.mar.manage.catalog.exception.ModelNotValideException;
import com.mar.manage.catalog.model.Category;
import com.mar.manage.catalog.repository.CategoryRepository;

@Component
public class CategoryBusiness {
	
	protected CategoryRepository categoryRepository;
	protected MapperDTO mapperDTO;
	
	@Autowired
	public CategoryBusiness(MapperDTO mapperDTO,
			CategoryRepository categoryRepository) {
		this.mapperDTO = mapperDTO;
		this.categoryRepository = categoryRepository;
	}
	
	// Create new category from categoryDTO
	public Category buildNewCategory(CategoryDTO categoryDTO) 
			throws ModelNotValideException {
		
		Category category = mapperDTO.convertToEntity(categoryDTO);
		
		// Set id, createDate and lastUpdatedDate
		category.setId(0);
		category.setCreateDate(new Date());
		category.setLastUpdateDate(null);
		
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return category;
	}
	
	// Create existed category from categoryDTO
	public Category buildExistedCategory(CategoryDTO categoryDTO) 
			throws CategoryNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !categoryRepository.existsById(categoryDTO.getId()) ) {
			throw new CategoryNotFoundException(Long.toString(categoryDTO.getId()));
		}
		
		// Build product from productDTO
		Category category = mapperDTO.convertToEntity(categoryDTO);
		
		// Set lastUpdateDate
		category.setLastUpdateDate(new Date());
		
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return category;
	}
	
	// Validate the product data
	public boolean isValide() {
		return true;
	}
}
