package com.mar.manage.catalog.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mar.manage.catalog.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	@Query("SELECT count(*) from Category")
	public int countCategories();
	
	public Optional<Category> findByName(String name);
	
	public List<Category> findAll();
}
