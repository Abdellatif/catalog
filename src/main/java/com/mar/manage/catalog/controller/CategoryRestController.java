package com.mar.manage.catalog.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.catalog.exception.ModelNotValideException;
import com.mar.manage.catalog.business.CategoryBusiness;
import com.mar.manage.catalog.dto.CategoryDTO;
import com.mar.manage.catalog.dto.MapperDTO;
import com.mar.manage.catalog.exception.CategoryNotFoundException;
import com.mar.manage.catalog.model.Category;
import com.mar.manage.catalog.repository.CategoryRepository;

@RestController
@RequestMapping("/categories")
public class CategoryRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryRestController.class);
	protected CategoryRepository categoryRepository;
	protected MapperDTO mapperDTO;
	protected CategoryBusiness categoryBusiness;
	
	@Autowired
	public CategoryRestController(CategoryRepository categoryRepository,
			MapperDTO mapperDTO,
			CategoryBusiness categoryBusiness) {
		
		LOGGER.info("CategoryRestController cosntructor has been called");
		this.categoryRepository = categoryRepository;
		this.mapperDTO = mapperDTO;
		this.categoryBusiness = categoryBusiness; 
	}

	/**
	 * @param name
	 * @return categories with name = Name
	 */
	// Get entity by name
	@GetMapping("/name/{name}")
	public ResponseEntity<CategoryDTO> getCategoryByName(@PathVariable("name") String name) 
			throws CategoryNotFoundException {
		
		LOGGER.info("Category-service byName() " + name);

		Optional<Category> category = categoryRepository.findByName(name);
		
		if( !category.isPresent() ) {
			throw new CategoryNotFoundException(name);
		}
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(category.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return category
	 */
	// Get entity by id
	@GetMapping("/{id}")
	public ResponseEntity<CategoryDTO> getCategoryById(@PathVariable("id") long id) 
			throws CategoryNotFoundException {
		
		LOGGER.info("Products-service byId() " + id);

		Optional<Category> category = categoryRepository.findById(id);
		
		if( !category.isPresent() ) {
			throw new CategoryNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(category.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all categories
	 */
	// Get entities
	@GetMapping("/")
	public ResponseEntity<List<CategoryDTO>> getAllCategories() {
		List<Category> result = categoryRepository.findAll();
		
		List<CategoryDTO> resultDTO = result.stream()
		          .map(item -> mapperDTO.convertToDTO(item))
		          .collect(Collectors.toList());
		
		return new ResponseEntity<>(
				resultDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param categoryDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Create entity
	@PostMapping("create")
	public ResponseEntity<CategoryDTO> saveCategory(@Valid @RequestBody CategoryDTO categoryDTO ) 
			throws MethodArgumentNotValidException, ModelNotValideException  {
		
		LOGGER.info("Categories-service Save");
		
		// Create category from categoryDTO 
		Category category = categoryBusiness.buildNewCategory(categoryDTO);
		
		categoryRepository.save(category);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(category),
				HttpStatus.OK);
	}
	
	/**
	 * @param categoryDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Update entity
	@PutMapping("update")
	public ResponseEntity<CategoryDTO> updateCategory(@Valid @RequestBody CategoryDTO categoryDTO ) 
			throws CategoryNotFoundException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Categories-service Save");
		
		// Create category from categoryDTO 
		Category category = categoryBusiness.buildExistedCategory(categoryDTO);

		categoryRepository.save(category);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(category),
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws CategoryNotFoundException {
		
		if ( !categoryRepository.existsById(id) ) {
			throw new CategoryNotFoundException((new Long(id)).toString());
		}
		
		categoryRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
}
