package com.mar.manage.catalog.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.catalog.business.ProductBusiness;
import com.mar.manage.catalog.dto.MapperDTO;
import com.mar.manage.catalog.dto.ProductDTO;
import com.mar.manage.catalog.exception.ModelNotValideException;
import com.mar.manage.catalog.exception.ProductNotFoundException;
import com.mar.manage.catalog.model.Product;
import com.mar.manage.catalog.repository.ProductRepository;


@RestController
@RequestMapping("/products")
public class ProductRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductRestController.class);
	protected ProductRepository productRepository;
	protected MapperDTO mapperDTO;
	protected ProductBusiness productBusiness;

	@Autowired
	public ProductRestController(ProductRepository productRepository,
			MapperDTO mapperDTO,
			ProductBusiness productBusiness) {
		
		LOGGER.info("ProductRestController cosntructor has been called");
		this.productRepository = productRepository;
		this.mapperDTO = mapperDTO;
		this.productBusiness = productBusiness;
	}

	/**
	 * @param name
	 * @return Products with name = Name
	 */
	// Get entity by name
	@GetMapping("/name/{name}")
	public ResponseEntity<ProductDTO> getProductByName(@PathVariable("name") String name) 
			throws ProductNotFoundException {
		
		LOGGER.info("Products-service byName() " + name);

		Optional<Product> product = productRepository.findByName(name);
		
		if( !product.isPresent() ) {
			throw new ProductNotFoundException(name);
		}
		
		return new ResponseEntity<>(
				this.mapperDTO.convertToDTO(product.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return Product
	 */
	// Get entity by id
	@GetMapping("/{id}")
	public ResponseEntity<ProductDTO> getProductById(@PathVariable("id") long id) 
			throws ProductNotFoundException {
		
		LOGGER.info("Products-service byId() " + id);

		Optional<Product> product = productRepository.findById(id);
		
		if( !product.isPresent() ) {
			throw new ProductNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(product.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all products
	 */
	// Get all entities
	@GetMapping("/")
	public ResponseEntity<List<ProductDTO>> getAllProducts() {
		List<Product> result = productRepository.findAll();
		
		List<ProductDTO> resultDTO = result.stream()
				.map(item -> mapperDTO.convertToDTO(item))
				.collect(Collectors.toList());
		
		return new ResponseEntity<>(
				resultDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param productDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Create entity
	@PostMapping("create")
	public ResponseEntity<ProductDTO> saveProduct(@Valid @RequestBody ProductDTO productDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Products-service Save");
		
		// Create product from productDTO
		Product product = this.productBusiness.buildNewProduct(productDTO);
		
		productRepository.save(product);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(product),
				HttpStatus.OK);
	}
	
	/**
	 * @param productDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Update entity
	@PutMapping("update")
	public ResponseEntity<ProductDTO> updateProduct(@Valid @RequestBody ProductDTO productDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Products-service update");
		
		// Create product from categoryDTO
		Product product = this.productBusiness.buildExistedProduct(productDTO);
		
		productRepository.save(product);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(product),
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws ProductNotFoundException {
		
		if ( !productRepository.existsById(id) ) {
			throw new ProductNotFoundException((new Long(id)).toString());
		}
		
		productRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
}
