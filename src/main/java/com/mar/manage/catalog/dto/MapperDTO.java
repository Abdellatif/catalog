package com.mar.manage.catalog.dto;

import java.util.Objects;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

import com.mar.manage.catalog.model.Category;
import com.mar.manage.catalog.model.Product;
import com.mar.manage.catalog.repository.CategoryRepository;
import com.mar.manage.catalog.repository.ProductRepository;

@Component
public final class MapperDTO {
	private ModelMapper modelMapper;
	private CategoryRepository categoryRepository;
	private ProductRepository productRepository;
	
	@Autowired
	public MapperDTO(ModelMapper modelMapper,
			CategoryRepository categoryRepository,
			ProductRepository productRepository) {
		this.modelMapper = modelMapper;
		this.categoryRepository = categoryRepository;
		this.productRepository = productRepository;
	}

	/********* Product Mapper ************/
	public ProductDTO convertToDTO(Product entity) {
		ProductDTO productDTO = this.modelMapper.map(entity, ProductDTO.class);
		
		// Set idCategory
		productDTO.setIdCategory( !Objects.isNull(entity.getCategory()) ? entity.getCategory().getId() : 0 );
		
		return productDTO;
	}
	
	public Product convertToEntity(ProductDTO dto) throws ParseException {
	    Product product = modelMapper.map(dto, Product.class);
	    
	    // Set category
	 	Optional<Category> category = categoryRepository.findById(dto.getIdCategory());
	 	product.setCategory( category.isPresent() ? category.get() : null );
	 	
	 	
	 	// Set createDate and lastUpdateDate
	 	Optional<Product> existedProduct = productRepository.findById(dto.getId());
	 	existedProduct.ifPresent( item -> {
	 		product.setCreateDate(item.getCreateDate());
	 		product.setLastUpdateDate(item.getLastUpdateDate());
	 	});
	 		
	    return product;
	}
	
	/********* Catalog Mapper ************/
	public CategoryDTO convertToDTO(Category entity) {
		return modelMapper.map(entity, CategoryDTO.class);
	}
	
	public Category convertToEntity(CategoryDTO dto) throws ParseException {
	    Category category = modelMapper.map(dto, Category.class);
	    
	    // Set createDate and lastUpdateDate
 	 	Optional<Category> existedCategory = categoryRepository.findById(dto.getId());
 	 	existedCategory.ifPresent( item -> {
 	 		category.setCreateDate(item.getCreateDate());
 	 		category.setLastUpdateDate(item.getLastUpdateDate());
 	 	});
	 	 	
	    return category;
	}
}
