CREATE TABLE Category(
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    name VARCHAR(100) NOT NULL,
	description VARCHAR(200) NULL,
    create_date DATE NULL,
    last_update_date DATE NULL,
);

CREATE TABLE Product (
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    name VARCHAR(100) NOT NULL,
    price float Null,
	description VARCHAR(200) NULL,
    create_date Date Null,
    last_update_date Date Null,
	category_id int not null,
	FOREIGN KEY (category_id) REFERENCES Category(id)
);


