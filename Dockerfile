FROM openjdk:8-jre-alpine

ADD ./target/catalog.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/catalog.jar"]

EXPOSE 9992
